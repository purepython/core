from core.db import DBQuery
from settings import PACKAGE


class CollectorObject(object):

    def __init__(self):
        self.collection = []

    def get(self, cls_name):
        sql = "SELECT {c}_id FROM {c}".format(c=cls_name.lower())
        pids = DBQuery().execute(sql)

        module = __import__(PACKAGE, fromlist=[cls_name])

        for pid in pids:
            model = getattr(module, cls_name)()
            vars(model)["{}_id".format(cls_name.lower())] = pid[0]
            model.select()
            self.collection.append(model)

        return self.collection
